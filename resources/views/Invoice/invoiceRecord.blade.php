<table class="table table-bordered invoiceRecord" id="laravel_crud">
    <thead>
    <tr>
        <th>Date</th>
        <th>Elasped Day</th>
        <th>Amount Due</th>

        <th>Principal Amount</th>
        <th>Minimum Interest</th>
        <th>Early Interest</th>
        <th>Late Interest</th>
        <th>Total Check</th>
    </tr>
    </thead>
    <tbody>
    @foreach($interestCalculations as $calculation)
    <tr>
        <td>{{ date('d/m/Y', strtotime($calculation->date)) }}</td>
        <td>{{ $calculation->elaspeDay }}</td>
        <td>{{ $calculation->amountDue }}</td>
        <td>{{ $calculation->principal }}</td>
        <td>{{ $calculation->minInterest }}</td>
        <td>{{ $calculation->earlyInterest }}</td>
        <td>{{ $calculation->lateInterest }}</td>
        <td>{{ $calculation->totalCheck }}</td>
    </tr>

    @endforeach
    </tbody>
</table>
<div style="display:flex;justify-content:center">
{{$interestCalculations ->links()}}
</div>
