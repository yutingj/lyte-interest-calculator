@extends('layouts.default')

@section('content')
<div>
    <div class="buttonRow">
        <a href="{{ route('invoices.edit',$invoice_info->id)}}" style="display:inline-block" class="btn btn-primary">Edit</a>
        <a href="" style="display:inline-block" class="btn btn-primary">Export to Document</a>

    </div>

    <div class="container">
        @include('invoice.invoiceDetailTable')

</div>
<div>
    <h4>Interest Calculation</h4>
    @include('invoice.invoiceRecord')
</div>
    <div style="padding-top: 20%"></div>
@endsection
