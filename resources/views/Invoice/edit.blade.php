@extends('layouts.default')

@section('content')
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.4/jquery-ui.js"></script>
<link href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.4/jquery-ui.css" rel="stylesheet">

<div class="buttonRow">
    <div class="text-right">
        <a href="{{ route('invoices.index') }}" class="btn btn-danger mb-2">Go Back</a>
    </div>
</div>
<hr />

<form action="{{ route('invoices.update', $invoice_info->id) }}" method="POST" name="update_invoice">
    @method('PATCH')
    {{ csrf_field() }}
    <div class="container">
        <div class="row">
            <div class="col">
                <div class="form-group">
                    <strong>Debtor</strong>
                    <select class="form-control" id="clientID" name="clientID" disabled="disabled">
                        <option value="{{$invoice_info->clientID}}">{{$client->name}}</option>
                    </select>
                </div>
                <div class="form-group">
                    <strong>Invoice No</strong>
                    <input type="text" name="id" class="form-control" value="{{$invoice_info->id}}" placeholder="Enter Invoice No" readonly="readonly">
                </div>
                <div class="form-group">
                    <strong>Invoice Date</strong>
                    <input type="text" id="date" class="form-control" value="{{date('d/m/Y', strtotime($invoice_info->date))}}" name="date" placeholder="Enter Invoice Date">
                    <span class="text-danger">{{ $errors->first('invoiceDate') }}</span>
                </div>
                <script type="text/javascript">
                    $( "#date" ).datepicker({
                        dateFormat: "dd/mm/yy",
                        onSelect: function() {
                            var date = jQuery("#date").val();
                            var term = jQuery("#term").val();
                            calculateDueDates(date,term);
                        }

                    })
                </script>
                <div class="form-group">
                    <strong>Invoice Amount</strong>
                    <input type="text" id="amount" value="{{ $invoice_info->amount}}" name="amount" class="form-control" placeholder="Enter Invoice Amount">
                    <span class="text-danger">{{ $errors->first('amount') }}</span>
                </div>
                <div class="form-group">
                    <strong>Invoice Term</strong>
                    <input type="text" id="term" value="{{ $invoice_info->term}}" name="term" class="form-control" placeholder="Enter Invoice Term">
                    <span class="text-danger">{{ $errors->first('term') }}</span>
                </div>
                <div class="form-group">
                    <strong>Minimum period</strong>
                    <input type="text" id="minPeriod" value="{{ $invoice_info->minPeriod}}" name="minPeriod" class="form-control" placeholder="Enter Minimum Period">
                    <span class="text-danger">{{ $errors->first('minPeriod') }}</span>
                </div>
                <div class="form-group">
                    <strong>Invoice Purchase Date</strong>
                    <input type="text" id="purchaseDate"  value="{{date('d/m/Y', strtotime($invoice_info->purchaseDate))}}"class="form-control" name="purchaseDate" placeholder="Enter Invoice Purchase Date">
                    <span class="text-danger">{{ $errors->first('purchaseDate') }}</span>
                </div>
                <script type="text/javascript">
                    $('#purchaseDate').datepicker({
                        dateFormat: "dd/mm/yy",
                        onSelect: function() {
                            var date = jQuery("#purchaseDate").val();
                            calculateMinDate(date);
                        }
                    });
                </script>
                <div class="form-group">
                    <strong>% Finance</strong>
                    <input type="text" id="financePerc" value="{{ $invoice_info->financePerc}}" name="financePerc" class="form-control" placeholder="Enter % Financed">
                    <span class="text-danger">{{ $errors->first('financePerc') }}</span>
                </div>
                <div class="form-group">
                    <strong>Grace Interest Rate</strong>
                    <input type="text" id="graceIntRate" value="{{ $invoice_info->graceIntRate}}" name="graceIntRate" class="form-control" placeholder="Enter Grace Interest Rate">
                    <span class="text-danger">{{ $errors->first('graceIntRate') }}</span>
                </div>
                <div class="form-group">
                    <strong>Late Interest Rate</strong>
                    <input type="text" id="lateIntRate" value="{{ $invoice_info->lateIntRate}}" name="lateIntRate" class="form-control" placeholder="Enter Late Interest Rate">
                    <span class="text-danger">{{ $errors->first('lateIntRate') }}</span>
                </div>
                <div class="form-group">
                    <strong>Minimum Period</strong>
                    <input type="text" id="minPeriod" value="{{ $invoice_info->minPeriod}}" name="minPeriod" class="form-control" placeholder="Enter Minimum Period">
                    <span class="text-danger">{{ $errors->first('minPeriod') }}</span>
                </div>
                <div class="form-group">
                    <strong>Buffer (Grace) Period</strong>
                    <input type="text" id="gracePeriod" value="{{ $invoice_info->gracePeriod}}" name="gracePeriod" class="form-control" placeholder="Enter Buffer (Grace) Period">
                    <span class="text-danger">{{ $errors->first('gracePeriod') }}</span>
                </div>
                <div class="form-group">
                    <strong>Late Payment Period</strong>
                    <input type="text" id="latePPeriod" value="{{ $invoice_info->latePPeriod}}" name="latePPeriod" class="form-control" placeholder="Enter Late Payment Period">
                    <span class="text-danger">{{ $errors->first('latePPeriod') }}</span>
                </div>
            </div>
            <div class="col">
                <div class="invoiceContainer">
                    <div class="row justify-content-center">
                        <div>
                            <p class="invoiceFieldText">Invoice Due Date:</p>
                        </div>
                        <div style="margin-left:15px">
                            <input type="text" id="dueDate"  value="{{date('d/m/Y', strtotime($invoice_info->dueDate))}}"name="dueDate" class="form-control" value="" readonly="readonly">
                        </div>
                    </div>

                    <div class="row justify-content-center">
                        <div>
                            <p class="invoiceFieldText">Advance Amount:</p>
                        </div>
                        <div style="margin-left:15px;">
                            <input type="text" id="advanceAmt" value="{{ $invoice_info->advanceAmt}} " name="advanceAmt" class="form-control" readonly="readonly">
                        </div>

                        <div class="row justify-content-center">
                            <div>
                                <p class="invoiceFieldText">30 Days From Purchase (Min Date):</p>
                            </div>
                            <div style="margin-left:15px;">
                                <input type="text" id="minDate" value="{{date('d/m/Y', strtotime($invoice_info->minDate))}}" name="minDate" class="form-control" readonly="readonly">
                            </div>
                        </div>
                        <div class="row justify-content-center">
                            <div>
                                <p class="invoiceFieldText">Payment Due Date:</p>
                            </div>
                            <div style="margin-left:15px;">
                                <input type="text" id="paymentDueDate" value="{{date('d/m/Y', strtotime($invoice_info->paymentDueDate))}}" name="paymentDueDate" class="form-control" readonly="readonly">
                            </div>
                        </div>
                    </div>
                    <div class="row justify-content-center">
                        <div>
                            <p class="invoiceFieldText">Default Due Date:</p>
                        </div>
                        <div style="margin-left:15px;">
                            <input type="text" id="defaultDue" name="defaultDue" class="form-control" placeholder="Enter Address" value="{{date('d/m/Y', strtotime($invoice_info->defaultDue))}}" readonly="readonly">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <button type="submit" class="btn btn-primary">Update</button>
                <a href="{{ route('invoices.index') }}" class="btn btn-danger">Cancel</a>
            </div>
        </div>
</form>
@endsection
