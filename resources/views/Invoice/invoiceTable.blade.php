@section('content')
<div>
 <div class="buttonRow">
    <a href="{{ route('invoices.create')}}" class="btn btn-success mb-2">Add</a>
 </div>

  <div class="">
      <table class="table table-bordered" id="laravel_crud">
      <thead>
      <tr>
          <th>Id</th>
          <th>Date</th>
          <th>Client Name</th>

          <th>Amount</th>
          <th>Due Date</th>
          <th>Advance Amount</th>
          <th>Payment Due Date</th>
          <th colspan="2" class="text-center">Action</th>
      </tr>
      </thead>
      <tbody>
      @foreach($invoices as $invoice)
      <tr>
          <td>{{ $invoice->id }}</td>
          <td>{{ date('d/m/Y', strtotime($invoice->date)) }}</td>
          <td>{{ $invoice->name }}</td>
          <td>{{ $invoice->amount }}</td>
          <td>{{ date('d/m/Y', strtotime($invoice->dueDate)) }}</td>
          <td>{{ $invoice->advanceAmt }}</td>
          <td>{{ date('d/m/Y', strtotime($invoice->paymentDueDate)) }}</td>
          <td class="">
              <div>
                  <a href="{{ route('invoices.show',$invoice->id)}}" style="display:inline-block" class="btn btn-primary">View</a>
                  <a href="{{ route('invoices.edit',$invoice->id)}}" style="display:inline-block" class="btn btn-primary">Edit</a>
                  <form action="{{ route('invoices.destroy', $invoice->id)}}" style="display:inline-block"  method="post">
                      {{ csrf_field() }}
                      @method('DELETE')
                      <button class="btn btn-danger" type="submit">Delete</button>
                  </form>
              </div>
          </td>
      </tr>
      @endforeach
      @if(count($invoices) < 1)
      <tr>
          <td colspan="10" class="text-center">There are no invoice available yet!</td>
          </td>
      </tr>
      @endif
      </tbody>
      </table>
      <div style="display:flex;justify-content:center">
          {{$invoices ->links()}}
      </div>
  </div>
    @endsection
