<html>
<head>
    <meta content="text/html; charset=UTF-8" http-equiv="content-type">
    <style
        type="text/css">
        @import url('https://themes.googleusercontent.com/fonts/css?kit=fpjTOVmNbO4Lz34iLyptLUXza5VhXqVC6o75Eld_V98');

        .lst-kix_list_4-1 > li {
            counter-increment: lst-ctn-kix_list_4-1
        }

        .lst-kix_list_3-1 > li {
            counter-increment: lst-ctn-kix_list_3-1
        }

        .lst-kix_list_2-1 > li {
            counter-increment: lst-ctn-kix_list_2-1
        }


        .lst-kix_list_1-1 > li {
            counter-increment: lst-ctn-kix_list_1-1
        }


        .lst-kix_list_3-0 > li:before {
            content: "" counter(lst-ctn-kix_list_3-0, decimal) ". "
        }


        .lst-kix_list_3-1 > li:before {
            content: "" counter(lst-ctn-kix_list_3-1, lower-latin) ". "
        }

        .lst-kix_list_3-2 > li:before {
            content: "" counter(lst-ctn-kix_list_3-2, lower-roman) ". "
        }


        .lst-kix_list_4-0 > li {
            counter-increment: lst-ctn-kix_list_4-0
        }

        ol.lst-kix_list_2-3.start {
            counter-reset: lst-ctn-kix_list_2-3 0
        }

        .lst-kix_list_3-5 > li:before {
            content: "" counter(lst-ctn-kix_list_3-5, lower-roman) ". "
        }

        .lst-kix_list_3-4 > li:before {
            content: "" counter(lst-ctn-kix_list_3-4, lower-latin) ". "
        }

        ol.lst-kix_list_1-5.start {
            counter-reset: lst-ctn-kix_list_1-5 0
        }

        .lst-kix_list_3-3 > li:before {
            content: "" counter(lst-ctn-kix_list_3-3, decimal) ". "
        }


        .lst-kix_list_3-8 > li:before {
            content: "" counter(lst-ctn-kix_list_3-8, lower-roman) ". "
        }

        .lst-kix_list_2-0 > li {
            counter-increment: lst-ctn-kix_list_2-0
        }

        .lst-kix_list_2-3 > li {
            counter-increment: lst-ctn-kix_list_2-3
        }

        .lst-kix_list_3-6 > li:before {
            content: "" counter(lst-ctn-kix_list_3-6, decimal) ". "
        }

        .lst-kix_list_4-3 > li {
            counter-increment: lst-ctn-kix_list_4-3
        }

        .lst-kix_list_3-7 > li:before {
            content: "" counter(lst-ctn-kix_list_3-7, lower-latin) ". "
        }

        ol.lst-kix_list_4-5.start {
            counter-reset: lst-ctn-kix_list_4-5 0
        }

        .lst-kix_list_1-2 > li {
            counter-increment: lst-ctn-kix_list_1-2
        }


        .lst-kix_list_3-2 > li {
            counter-increment: lst-ctn-kix_list_3-2
        }



        .lst-kix_list_1-4 > li {
            counter-increment: lst-ctn-kix_list_1-4
        }

        .lst-kix_list_4-4 > li {
            counter-increment: lst-ctn-kix_list_4-4
        }

        ol.lst-kix_list_2-0 {
            list-style-type: none
        }

        ol.lst-kix_list_1-6.start {
            counter-reset: lst-ctn-kix_list_1-6 0
        }

        ol.lst-kix_list_2-1 {
            list-style-type: none
        }

        .lst-kix_list_4-8 > li:before {
            content: "" counter(lst-ctn-kix_list_4-8, lower-roman) ". "
        }

        .lst-kix_list_4-7 > li:before {
            content: "" counter(lst-ctn-kix_list_4-7, lower-latin) ". "
        }

        .lst-kix_list_3-0 > li {
            counter-increment: lst-ctn-kix_list_3-0
        }

        .lst-kix_list_3-3 > li {
            counter-increment: lst-ctn-kix_list_3-3
        }


        .lst-kix_list_3-6 > li {
            counter-increment: lst-ctn-kix_list_3-6
        }

        .lst-kix_list_2-5 > li {
            counter-increment: lst-ctn-kix_list_2-5
        }

        .lst-kix_list_2-8 > li {
            counter-increment: lst-ctn-kix_list_2-8
        }


        .lst-kix_list_2-2 > li {
            counter-increment: lst-ctn-kix_list_2-2
        }

        .lst-kix_list_2-6 > li:before {
            content: "" counter(lst-ctn-kix_list_2-6, decimal) ". "
        }

        .lst-kix_list_2-7 > li:before {
            content: "" counter(lst-ctn-kix_list_2-7, lower-latin) ". "
        }

        .lst-kix_list_2-7 > li {
            counter-increment: lst-ctn-kix_list_2-7
        }

        .lst-kix_list_3-7 > li {
            counter-increment: lst-ctn-kix_list_3-7
        }

        .lst-kix_list_2-4 > li:before {
            content: "" counter(lst-ctn-kix_list_2-4, lower-latin) ". "
        }

        .lst-kix_list_2-5 > li:before {
            content: "" counter(lst-ctn-kix_list_2-5, lower-roman) ". "
        }

        .lst-kix_list_2-8 > li:before {
            content: "" counter(lst-ctn-kix_list_2-8, lower-roman) ". "
        }


        .lst-kix_list_4-7 > li {
            counter-increment: lst-ctn-kix_list_4-7
        }

        .lst-kix_list_1-7 > li {
            counter-increment: lst-ctn-kix_list_1-7
        }

        .lst-kix_list_4-0 > li:before {
            content: "" counter(lst-ctn-kix_list_4-0, decimal) ". "
        }

        .lst-kix_list_2-6 > li {
            counter-increment: lst-ctn-kix_list_2-6
        }

        .lst-kix_list_3-8 > li {
            counter-increment: lst-ctn-kix_list_3-8
        }

        .lst-kix_list_4-1 > li:before {
            content: "" counter(lst-ctn-kix_list_4-1, lower-latin) ". "
        }

        .lst-kix_list_4-6 > li {
            counter-increment: lst-ctn-kix_list_4-6
        }

        .lst-kix_list_4-4 > li:before {
            content: "" counter(lst-ctn-kix_list_4-4, lower-latin) ". "
        }

        .lst-kix_list_1-5 > li {
            counter-increment: lst-ctn-kix_list_1-5
        }

        .lst-kix_list_4-3 > li:before {
            content: "" counter(lst-ctn-kix_list_4-3, decimal) ". "
        }

        .lst-kix_list_4-5 > li:before {
            content: "" counter(lst-ctn-kix_list_4-5, lower-roman) ". "
        }

        .lst-kix_list_4-2 > li:before {
            content: "" counter(lst-ctn-kix_list_4-2, lower-roman) ". "
        }

        .lst-kix_list_4-6 > li:before {
            content: "" counter(lst-ctn-kix_list_4-6, decimal) ". "
        }

        .lst-kix_list_1-8 > li {
            counter-increment: lst-ctn-kix_list_1-8
        }


        .lst-kix_list_3-5 > li {
            counter-increment: lst-ctn-kix_list_3-5
        }


        .lst-kix_list_3-4 > li {
            counter-increment: lst-ctn-kix_list_3-4
        }

        .lst-kix_list_2-4 > li {
            counter-increment: lst-ctn-kix_list_2-4
        }


        .lst-kix_list_1-0 > li:before {
            content: "" counter(lst-ctn-kix_list_1-0, decimal) ". "
        }


        .lst-kix_list_1-1 > li:before {
            content: "" counter(lst-ctn-kix_list_1-1, lower-latin) ". "
        }

        .lst-kix_list_1-2 > li:before {
            content: "" counter(lst-ctn-kix_list_1-2, lower-roman) ". "
        }


        .lst-kix_list_1-3 > li:before {
            content: "" counter(lst-ctn-kix_list_1-3, decimal) ". "
        }

        .lst-kix_list_1-4 > li:before {
            content: "" counter(lst-ctn-kix_list_1-4, lower-latin) ". "
        }


        .lst-kix_list_1-0 > li {
            counter-increment: lst-ctn-kix_list_1-0
        }

        .lst-kix_list_4-8 > li {
            counter-increment: lst-ctn-kix_list_4-8
        }

        .lst-kix_list_1-6 > li {
            counter-increment: lst-ctn-kix_list_1-6
        }

        .lst-kix_list_1-7 > li:before {
            content: "" counter(lst-ctn-kix_list_1-7, lower-latin) ". "
        }


        .lst-kix_list_1-3 > li {
            counter-increment: lst-ctn-kix_list_1-3
        }

        .lst-kix_list_1-5 > li:before {
            content: "" counter(lst-ctn-kix_list_1-5, lower-roman) ". "
        }

        .lst-kix_list_1-6 > li:before {
            content: "" counter(lst-ctn-kix_list_1-6, decimal) ". "
        }

        .lst-kix_list_2-0 > li:before {
            content: "" counter(lst-ctn-kix_list_2-0, decimal) ". "
        }

        .lst-kix_list_2-1 > li:before {
            content: "" counter(lst-ctn-kix_list_2-1, lower-latin) ". "
        }


        .lst-kix_list_4-5 > li {
            counter-increment: lst-ctn-kix_list_4-5
        }

        .lst-kix_list_1-8 > li:before {
            content: "" counter(lst-ctn-kix_list_1-8, lower-roman) ". "
        }

        .lst-kix_list_2-2 > li:before {
            content: "" counter(lst-ctn-kix_list_2-2, lower-roman) ". "
        }

        .lst-kix_list_2-3 > li:before {
            content: "" counter(lst-ctn-kix_list_2-3, decimal) ". "
        }

        .lst-kix_list_4-2 > li {
            counter-increment: lst-ctn-kix_list_4-2
        }

        ol {
            margin: 0;
            padding: 0
        }


        .c5 {
            padding-top: 0pt;
            padding-bottom: 5pt;
            line-height: 1.0;
            orphans: 2;
            widows: 2;
            text-align: left;
            height: 10pt
        }

        .c0 {
            color: #000000;
            font-weight: 400;
            text-decoration: none;
            vertical-align: baseline;
            font-size: 10pt;
            font-family: "Arial";
            font-style: normal
        }

        .c3 {
            color: #808080;
            font-weight: 700;
            text-decoration: none;
            vertical-align: baseline;
            font-size: 8pt;
            font-family: "Arial";
            font-style: normal
            text-align:right;
        }

        .c8 {
            padding-top: 0pt;
            padding-bottom: 0pt;
            line-height: 1.0;
            orphans: 2;
            widows: 2;
            text-align: left;
            height: 10pt
        }

        .c7 {
            color: #000000;
            font-weight: 400;
            text-decoration: none;
            vertical-align: baseline;
            font-size: 10pt;
            font-family: "Arial";
            font-style: normal
        }

        .c13 {
            color: #000000;
            font-weight: 700;
            text-decoration: none;
            vertical-align: baseline;
            font-size: 10pt;
            font-family: "Arial";
            font-style: normal
        }

        .c2 {
            padding-top: 0pt;
            padding-bottom: 5pt;
            line-height: 1.0;
            orphans: 2;
            widows: 2;
            text-align: left;
            margin-right: 4.5pt
        }


        .noBorder, th, td {
            border: 0px solid white !important;
        }

        .th , td {
            width:50%;
        }

        .headerRepayment {
            font-weight:800;
            padding-top: 40px;
            padding-bottom: 40px;
        }
        .table, th, td {
            border: 1px solid black;
            border-collapse: collapse !important;
            font-size:13px;
            font-family: "Arial";
            padding:2;
        }

        .tableCollaspe {
            border-collapse: collapse;
            text-align:left;
            font-family: "Arial";
            width:50%;
            font-size:15px
        }

        .table td, table th {
            padding:5;
        }

        .c20 {
            color: #808080;
            font-weight: 700;
            text-decoration: none;
            vertical-align: baseline;
            font-size: 14pt;
            font-family: "Arial";
            font-style: normal
        }

        .c16 {
            padding-top: 0pt;
            padding-bottom: 5pt;
            line-height: 1.0;
            orphans: 2;
            widows: 2;
            text-align: left
        }

        .c4 {
            padding-top: 0pt;
            padding-bottom: 0pt;
            line-height: 1.0;
            orphans: 2;
            widows: 2;
            text-align: right
        }

        .c9 {
            padding-top: 0pt;
            padding-bottom: 0pt;
            line-height: 1.0;
            orphans: 2;
            widows: 2;
            text-align: center
        }

        .c33 {
            padding-top: 0pt;
            padding-bottom: 5pt;
            line-height: 1.0791666666666666;
            orphans: 2;
            widows: 2;
            text-align: left
        }

        .c18 {
            padding-top: 0pt;
            padding-bottom: 0pt;
            line-height: 1.0;
            orphans: 2;
            widows: 2;
            text-align: left
        }

        .c24 {
            padding-top: 0pt;
            padding-bottom: 0pt;
            line-height: 1.15;
            text-align: left;
            height: 10pt
        }


        .c35 {
            background-color: #ffffff;
            max-width: 451pt;
            padding: 72pt 72pt 72pt 72pt
        }

        .c12 {
            font-weight: 400;
            font-family: "Arial"
        }

        .c17 {
            font-size: 8pt;
            color: #808080
        }



        .border {
            border: 1px solid black !important;
        }


        .c32 {
            height: 10pt
        }



        .c22 {
            margin-right: 4.5pt
        }

        .c23 {
            margin-right: -52pt
        }


        li {
            color: #000000;
            font-size: 10pt;
            font-family: "Arial"
        }

        p {
            margin: 0;
            color: #000000;
            font-size: 10pt;
            font-family: "Arial"
        }

        h1 {
            padding-top: 24pt;
            color: #000000;
            font-weight: 700;
            font-size: 24pt;
            padding-bottom: 6pt;
            font-family: "Arial";
            line-height: 1.0;
            page-break-after: avoid;
            orphans: 2;
            widows: 2;
            text-align: left
        }

        h2 {
            padding-top: 18pt;
            color: #000000;
            font-weight: 700;
            font-size: 18pt;
            padding-bottom: 4pt;
            font-family: "Arial";
            line-height: 1.0;
            page-break-after: avoid;
            orphans: 2;
            widows: 2;
            text-align: left
        }

        h3 {
            padding-top: 14pt;
            color: #000000;
            font-weight: 700;
            font-size: 14pt;
            padding-bottom: 4pt;
            font-family: "Arial";
            line-height: 1.0;
            page-break-after: avoid;
            orphans: 2;
            widows: 2;
            text-align: left
        }

        h4 {
            padding-top: 12pt;
            color: #000000;
            font-weight: 700;
            font-size: 12pt;
            padding-bottom: 2pt;
            font-family: "Arial";
            line-height: 1.0;
            page-break-after: avoid;
            orphans: 2;
            widows: 2;
            text-align: left
        }

        h5 {
            padding-top: 11pt;
            color: #000000;
            font-weight: 700;
            font-size: 11pt;
            padding-bottom: 2pt;
            font-family: "Arial";
            line-height: 1.0;
            page-break-after: avoid;
            orphans: 2;
            widows: 2;
            text-align: left
        }

        h6 {
            padding-top: 10pt;
            color: #000000;
            font-weight: 700;
            font-size: 10pt;
            padding-bottom: 2pt;
            font-family: "Arial";
            line-height: 1.0;
            page-break-after: avoid;
            orphans: 2;
            widows: 2;
            text-align: left
        }</style>
</head>
<body class="c35">
<div><p class="c9"><span class="c3">PRIVATE AND CONFIDENTIAL</span></p>
    <p style="height:10px"></p>
    <p class="c4" style="text-align: left">
        <span
            style="overflow: hidden; display: inline-block; margin: 0.00px 0.00px; border: 0.00px solid #000000; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px); width: 225.27px; height: 81.93px;">
            <img
                alt="" src="img/image1.png"
                style="width: 225.27px; height: 81.93px; margin-left: -0.00px; margin-top: -0.00px; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px);"
                title="">
        </span></p>
</div>
<div style="width:100%;text-align: right"><span class="c3">FUNDFLOW PTE LTD</span></div>
<p class="c4"><span class="c12 c17">UEN: 201820899W</span></p>
<p class="c4"><span class="c12 c17">6 Bukit Pasoh Road, Singapore 089820</span></p>
<p class="c9 c32"><span class="c20"></span></p>
<p class="c9 c32"><span class="c20"></span></p>
<p class="c9"><span class="c20">NOTICE OF REPAYMENT</span></p>
<p class="c8"><span class="c0"></span></p></div>


<p class="c16"><span class="c0">{{ $invoice_info->name}}</span></p>
<p class="c16"><span class="c0">Date: {{ date('d/m/Y', strtotime($invoice_info->purchaseDate)) }}</span></p>
<p class="c5"><span class="c0"></span></p>
<p class="c16"><span class="c0">Dear Sir,</span></p>
<p class="c5"><span class="c0"></span></p>
<p class="c16"><span class="c0">With reference to the debt purchased on {{ $invoice_info->purchaseDate}} for SGD {{$invoice_info->advanceAmt }}. We set forth here with the purchase details and the amount payable in accordance with the bank details below:</span>
</p>
<p class="c5"><span class="c0"></span></p>
<p class="c5 c23"><span class="c0"></span></p>

<table cellspacing="0" class="noBorder">
    <tr>
        <td>Beneficiary Account Name:</td>
        <td>FUNDFLOW PTE. LTD.</td>
    </tr>
    <tr>
        <td>Beneficiary Account Number (IBAN):</td>
        <td>014-9046-997 (SGD Account) </td>
    </tr>
    <tr>
        <td>Beneficiary Bank:</td>
        <td>The Development Bank of Singapore Limited (DBS)</td>
    </tr>
    <tr style="height:50px">
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td>Disbursement Date:</td>
        <td>{{ date('d/m/Y', strtotime($invoice_info->purchaseDate)) }}</td>
    </tr>
    <tr>
        <td>Purchase Currency:</td>
        <td>SGD</td>
    </tr>
    <tr>
        <td>Total Advanced Amount:</td>
        <td>${{ $invoice_info->advanceAmt }}</td>
    </tr>
    <tr>
        <td>Processing Fee:</td>
        <td>1.00%</td>
    </tr>
    <tr>
        <td>Facility Fee:</td>
        <td>NA</td>
    </tr>
    <tr>
        <td>Total Disbursement Amount:</td>
        <td>${{ $invoice_info->amount}}</td>
    </tr>
    <tr>
        <td></td>
        <td></td>
    </tr>
    <tr style="height:50px">
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td>Monthly Rate incl. grace period due date:</td>
        <td>{{ $invoice_info->graceIntRate }}% per 30 days</td>
    </tr>
    <tr>
        <td>Discount per month after grace period due date:</td>
        <td>{{ $invoice_info->lateIntRate }}% per 30 days</td>
    </tr>
</table>
<div style="height:90px"></div>
<p class="c18"><span class="c0">Yours faithfully </span></p>
<p class="c18"><span>for and on behalf of</span><span class="c13"> FUNDFLOW PTE LTD</span></p>
<div style="height:35px"></div>
<div><p class="c9"><span class="c3">PRIVATE AND CONFIDENTIAL</span></p>
    <p style="height:10px"></p>
    <p class="c4" style="text-align: left">
        <span
            style="overflow: hidden; display: inline-block; margin: 0.00px 0.00px; border: 0.00px solid #000000; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px); width: 225.27px; height: 81.93px;">
            <img
                alt="" src="img/image1.png"
                style="width: 225.27px; height: 81.93px; margin-left: -0.00px; margin-top: -0.00px; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px);"
                title="">
        </span></p>
</div>
<div style="width:100%;text-align: right"><span class="c3">FUNDFLOW PTE LTD</span></div>
<p class="c4"><span class="c12 c17">UEN: 201820899W</span></p>
<p class="c4"><span class="c12 c17">6 Bukit Pasoh Road, Singapore 089820</span></p>
<p class="c9 c32"><span class="c20"></span></p>
<p class="c9 c32"><span class="c20"></span></p>
<p class="c9"><span class="c20">NOTICE OF REPAYMENT</span></p>
<p class="c5 c22"><span class="c0"></span></p>
<div style="height:100px"></div>

<table cellspacing="0" style="width:100%"class="noBorder">
    <tr>
        <td>Debtor:</td>
        <td>{{ $invoice_info->name}}</td>
    </tr>
    <tr>
        <td>Invoice Number:</td>
        <td>{{ $invoice_info->id}}</td>
    </tr>
    <tr>
        <td>Invoice Date:</td>
        <td>{{ date('d/m/Y', strtotime($invoice_info->date)) }}</td>
    </tr>
    <tr>
        <td>Invoice Terms:</td>
        <td>{{ $invoice_info->term}} days</td>
    </tr>
    <tr>
        <td>Invoice Due Date:</td>
        <td>{{ date('d/m/Y', strtotime($invoice_info->dueDate)) }}</td>
    </tr>
    <tr style="height:50px">
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td>Invoice Purchase Date:</td>
        <td>{{ date('d/m/Y', strtotime($invoice_info->purchaseDate)) }}</td>
    </tr>
    <tr>
        <td>Percentage Financed:</td>
        <td>{{ $invoice_info->financePerc}}%</td>
    </tr>
    <tr>
        <td>Amount Advanced:</td>
        <td>${{ $invoice_info->advanceAmt}}</td>
    </tr>
    <tr>
        <td>30 days from Date Purchased: </td>
        <td>{{ date('d/m/Y', strtotime($minDate->minDate)) }}</td>
    </tr>
    <tr>
        <td>Grace Period:</td>
        <td>{{ $invoice_info->gracePeriod }} days</td>
    </tr>
    <tr>
        <td>Payment Due Date:</td>
        <td>{{ $invoice_info->paymentDueDate}}</td>
    </tr>
</table>

<p class="headerRepayment">Repayment Schedule (30 days from date of purchase):</p>
<table class="tableCollaspe">
    <tr class="border">
        <th class="border">Date</th>
        <th class="border">Amount Due ($)</th>
    </tr>
    @foreach($thirtyDays as $thirtyDay)
    <tr class="border">
        <td class="border">{{ date('d/m/Y', strtotime($thirtyDay->date)) }}</td>
        <td class="border">{{$thirtyDay->amountDue}}</td>
    </tr>
    @endforeach
</table>

<div style="height:50px"></div>
<div><p class="c9"><span class="c3">PRIVATE AND CONFIDENTIAL</span></p>
    <p style="height:10px"></p>
    <p class="c4" style="text-align: left">
        <span
            style="overflow: hidden; display: inline-block; margin: 0.00px 0.00px; border: 0.00px solid #000000; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px); width: 225.27px; height: 81.93px;">
            <img
                alt="" src="img/image1.png"
                style="width: 225.27px; height: 81.93px; margin-left: -0.00px; margin-top: -0.00px; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px);"
                title="">
        </span></p>
</div>
<div style="width:100%;text-align: right"><span class="c3">FUNDFLOW PTE LTD</span></div>
<p class="c4"><span class="c12 c17">UEN: 201820899W</span></p>
<p class="c4"><span class="c12 c17">6 Bukit Pasoh Road, Singapore 089820</span></p>
<p class="c9 c32"><span class="c20"></span></p>
<p class="c9 c32"><span class="c20"></span></p>
<p class="c9"><span class="c20">NOTICE OF REPAYMENT</span></p>
<p class="c8"><span class="c0"></span></p></div>

<p class="headerRepayment">Repayment Schedule (Before Late Payment Interest):</p>
<table class="tableCollaspe">
    <tr class="border">
        <th class="border">Date</th>
        <th class="border">Amount Due ($)</th>
    </tr>
    @foreach($beforeLateRepayments as $bLateRepayment)
    <tr class="border">
        <td class="border">{{ date('d/m/Y', strtotime($bLateRepayment->date)) }}</td>
        <td class="border">{{$bLateRepayment->amountDue}}</td>
    </tr>
    @endforeach

</table>

<p class="headerRepayment">Repayment Schedule (Inclusive of Late Payment Interest):</p>
<table class="tableCollaspe">
    <tr class="border">
        <th class="border">Date</th>
        <th class="border">Amount Due ($)</th>
    </tr>
    @foreach($lateRepayments as $lateRepayment)
    <tr class="border">
        <td class="border">{{ date('d/m/Y', strtotime($lateRepayment->date)) }}</td>
        <td class="border">{{$lateRepayment->amountDue}}</td>
    </tr>
    @endforeach
</table>

<p class="c5 c22"><span class="c7"></span></p>
<p class="c2"><span
        class="c7">Please contact us if you want to find out the amount due for other dates not listed here</span></p>
</body>
</html>
