@extends('layouts.default')

@section('content')
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.4/jquery-ui.js"></script>
<link href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.4/jquery-ui.css" rel="stylesheet">

<div class="buttonRow">
    <div class="text-right">
        <a href="{{ route('invoices.index') }}" class="btn btn-danger mb-2">Go Back</a>
    </div>
</div>
<hr />

<form action="{{ route('invoices.store') }}" method="POST" name="add_invoice">
    {{ csrf_field() }}
<div class="container">
  <div class="row">
    <div class="col">
      <div class="form-group">
          <strong>Debtor</strong>
          <select class="form-control" id="clientID" name="clientID">
              @foreach($clients as $client)
                <option value="{{$client->id}}">{{$client->name}}</option>
              @endforeach
          </select>
      </div>
      <div class="form-group">
          <strong>Invoice No</strong>
          <input type="text" name="id" class="form-control" placeholder="Enter Invoice No" value="{{ old('id') }}">
          <span class="text-danger">{{ $errors->first('id') }}</span>
      </div>
        <div class="form-group">
            <strong>Invoice Date</strong>
            <input type="text" id="date" class="form-control" name="date" placeholder="Enter Invoice Date" value="{{ old('date') }}">
            <span class="text-danger">{{ $errors->first('date') }}</span>
        </div>
        <script type="text/javascript">
            $( "#date" ).datepicker({
                dateFormat: "dd/mm/yy",
                onSelect: function() {
                    var date = jQuery("#date").val();
                    var term = jQuery("#term").val();
                    calculateDueDates(date,term);
                }
            })
        </script>

      <div class="form-group">
          <strong>Invoice Amount</strong>
          <input type="text" id="amount" name="amount" class="form-control" placeholder="Enter Invoice Amount" value="{{ old('amount') }}">
          <span class="text-danger">{{ $errors->first('amount') }}</span>
      </div>
        <div class="form-group">
          <strong>Invoice Term</strong>
          <input type="text" id="term" name="term" class="form-control" placeholder="Enter Invoice Term" value="{{ old('term') }}">
          <span class="text-danger">{{ $errors->first('term') }}</span>
      </div>
      <div class="form-group">
            <strong>Minimum period</strong>
            <input type="text" id="minPeriod" name="minPeriod" class="form-control" placeholder="Enter Minimum Period" value="{{ old('minPeriod') }}">
            <span class="text-danger">{{ $errors->first('minPeriod') }}</span>
      </div>
        <div class="form-group">
            <strong>Invoice Purchase Date</strong>
            <input type="text" id="purchaseDate" class="form-control" name="purchaseDate" placeholder="Enter Invoice Purchase Date" value="{{ old('purchaseDate') }}">
            <span class="text-danger">{{ $errors->first('purchaseDate') }}</span>
        </div>
        <script type="text/javascript">
            $('#purchaseDate').datepicker({
                dateFormat: "dd/mm/yy",
                onSelect: function() {
                    var date = jQuery("#purchaseDate").val();
                    calculateMinDate(date);
                }
            });
        </script>
      <div class="form-group">
              <strong>% Finance</strong>
              <input type="text" id="financePerc" name="financePerc" class="form-control" placeholder="Enter % Financed" value="{{ old('financePerc') }}">
              <span class="text-danger">{{ $errors->first('financePerc') }}</span>
        </div>
      <div class="form-group">
              <strong>Grace Interest Rate</strong>
              <input type="text" id="graceIntRate" name="graceIntRate" class="form-control" placeholder="Enter Grace Interest Rate" value="{{ old('graceIntRate') }}">
              <span class="text-danger">{{ $errors->first('graceIntRate') }}</span>
        </div>
      <div class="form-group">
              <strong>Late Interest Rate</strong>
              <input type="text" id="lateIntRate" name="lateIntRate" class="form-control" placeholder="Enter Late Interest Rate" value="{{ old('lateIntRate') }}">
              <span class="text-danger">{{ $errors->first('lateIntRate') }}</span>
        </div>
     <div class="form-group">
          <strong>Minimum Period</strong>
          <input type="text" id="minPeriod" name="minPeriod" class="form-control" placeholder="Enter Minimum Period" value="{{ old('minPeriod') }}">
          <span class="text-danger">{{ $errors->first('minPeriod') }}</span>
     </div>
     <div class="form-group">
           <strong>Buffer (Grace) Period</strong>
           <input type="text" id="gracePeriod" name="gracePeriod" class="form-control" placeholder="Enter Buffer (Grace) Period" value="{{ old('gracePeriod') }}">
           <span class="text-danger">{{ $errors->first('gracePeriod') }}</span>
      </div>
      <div class="form-group">
         <strong>Late Payment Period</strong>
         <input type="text" id="latePPeriod" name="latePPeriod" class="form-control" placeholder="Enter Late Payment Period" value="{{ old('latePPeriod') }}">
         <span class="text-danger">{{ $errors->first('latePPeriod') }}</span>
    </div>
    </div>
    <div class="col">
        <div class="invoiceContainer">
         <div class="row justify-content-center">
              <div>
              <p class="invoiceFieldText">Invoice Due Date:</p>
              </div>
              <div style="margin-left:15px">
              <input type="text" id="dueDate" name="dueDate" class="form-control" value="" readonly="readonly" value="{{ old('dueDate') }}">
              </div>
          </div>

         <div class="row justify-content-center">
              <div>
              <p class="invoiceFieldText">Advance Amount:</p>
              </div>
              <div style="margin-left:15px;">
              <input type="text" id="advanceAmt" name="advanceAmt" class="form-control" readonly="readonly" value="{{ old('advanceAmt') }}">
           </div>

         <div class="row justify-content-center">
            <div>
            <p class="invoiceFieldText">30 Days From Purchase (Min Date):</p>
            </div>
            <div style="margin-left:15px;">
            <input type="text" id="minDate" name="minDate" class="form-control" readonly="readonly" value="{{ old('minDate') }}">
            </div>
            </div>
        <div class="row justify-content-center">
            <div>
            <p class="invoiceFieldText">Payment Due Date:</p>
            </div>
            <div style="margin-left:15px;">
            <input type="text" id="paymentDueDate" name="paymentDueDate" class="form-control" readonly="readonly" value="{{ old('paymentDueDate') }}">
            </div>
            </div>
        </div>
        <div class="row justify-content-center">
            <div>
            <p class="invoiceFieldText">Default Due Date:</p>
            </div>
            <div style="margin-left:15px;">
            <input type="text" id="defaultDue" name="defaultDue" class="form-control" placeholder="Enter Address" value="27/5/2010" readonly="readonly" value="{{ old('defaultDue') }}">
            </div>
            </div>
        </div>
      </div>
  </div>
    <div class="row">
        <div class="col-md-12">
            <button type="submit" class="btn btn-primary">Create</button>
            <a href="{{ route('invoices.index') }}" class="btn btn-danger">Cancel</a>
        </div>
    </div>
</form>
@endsection
