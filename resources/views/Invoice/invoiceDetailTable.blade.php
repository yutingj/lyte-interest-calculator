<table class="table table-bordered invoiceTable" id="laravel_crud">
    <thead>
    </thead>
    <tbody>

    <tr>
    <td>Debtor</td>
    <td>{{ $invoice_info->client->name }} </td>
    </tr>
    <tr>
        <td>Invoice No</td>
        <td>{{ $invoice_info->id}}</td>
    </tr>
    <tr>
        <td>Invoice Date</td>
        <td>{{ date('d/m/Y', strtotime($invoice_info->date)) }}</td>
    </tr>
    <tr>
        <td>Invoice Amount</td>
        <td>{{ $invoice_info->amount}}</td>
    </tr>
    <tr>
        <td>Invoice Term</td>
        <td>{{ $invoice_info->term}}</td>
    </tr>
    <tr>
        <td>Minimum Period</td>
        <td>{{ $invoice_info->minPeriod}}</td>
    </tr>
    <tr>
        <td>Invoice Purchase Date</td>
        <td>{{ date('d/m/Y', strtotime($invoice_info->purchaseDate)) }}</td>
    </tr>
    <tr>
        <td>% Financed</td>
        <td>{{ $invoice_info->financePerc}}</td>
    </tr>
    <tr>
        <td>Grace Interest Rate</td>
        <td>{{ $invoice_info->graceIntRate }}</td>
    </tr>
    <tr>
        <td>Late Interest Rate </td>
        <td>{{ $invoice_info->lateIntRate}} </td>
    </tr>
    <tr>
        <td>Minimum Period (Days)</td>
        <td>{{ $invoice_info->minPeriod}}</td>
    </tr>
    <tr>
        <td>Invoice Due Date</td>
        <td>{{ date('d/m/Y', strtotime($invoice_info->dueDate)) }}</td>
    </tr>
    <tr>
        <td>Advance Amount</td>
        <td>{{ $invoice_info->advanceAmt}}</td>
    </tr>
    <tr>
        <td>30 Days From Purchase (Min Date)</td>
        <td>{{ date('d/m/Y', strtotime($invoice_info->minDate)) }}</td>
    </tr>
    <tr>
        <td>Payment Due Date</td>
        <td>{{ date('d/m/Y', strtotime($invoice_info->paymentDueDate)) }}</td>
    </tr>
    <tr>
        <td>Default Due Date</td>
        <td>{{ date('d/m/Y', strtotime($invoice_info->defaultDue)) }}</td>
    </tr>

    </tbody>
</table>
