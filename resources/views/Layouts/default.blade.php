@extends('app')

<!doctype html>
<html>
<head>
    @include('layouts.navbar')
</head>
<body>
<div class="container">

    <header>
    <h2>{{ $title }}</h2>
    </header>

    <div id="main">
    @if(session()->has('success'))
    <div class="alert alert-success">
        {{ session()->get('success') }}
    </div>
    @endif
         @yield('content')
    </div>

    <footer class="row">
    </footer>

</div>
</body>
</html>
