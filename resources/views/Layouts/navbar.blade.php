<div>
<nav class="navbar navbar-expand-lg " color-on-scroll="500">
    <div class="container-fluid">
        <a class="navbar-brand" href="#">LyteFinance </a>
        <div class="collapse navbar-collapse justify-content-end" id="navigation">
            <ul class="nav navbar-nav mr-auto">
                <li class="nav-item">
                   <a href="/#" class="nav-link">
                       Home
                   </a>
                </li>
                <li class="nav-item">
                   <a href="{{ route('clients.index') }}" class="nav-link">
                       Clients
                   </a>
                </li>
                <li class="nav-item">
                   <a href="{{ route('invoices.index') }}" class="nav-link">
                       Invoices
                   </a>
                </li>
            </ul>
        </div>
    </div>
</nav>
</div>
