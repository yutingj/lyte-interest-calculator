@extends('layouts.default')

@section('content')
<div class="buttonRow">
    <div class="text-right">
        <a href="{{ route('clients.index') }}" class="btn btn-danger mb-2">Go Back</a>
    </div>
</div>
<hr />

<form action="{{ route('clients.store') }}" method="POST" name="add_client">
    {{ csrf_field() }}

    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <strong>Name</strong>
                <input type="text" name="name" class="form-control" placeholder="Enter name">
                <span class="text-danger">{{ $errors->first('name') }}</span>
            </div>
        </div>
        <div class="col-md-12">
            <div class="form-group">
                <strong>Postal Code</strong>
                <input type="text" name="postalCode" class="form-control" placeholder="Enter Postal Code">
                <span class="text-danger">{{ $errors->first('postalCode') }}</span>
            </div>
        </div>
        <div class="col-md-12">
            <div class="form-group">
                <strong>Address</strong>
                <input type="text" name="address" class="form-control" placeholder="Enter Address">
                <span class="text-danger">{{ $errors->first('address') }}</span>
            </div>
        </div>

        <div class="col-md-12">
            <button type="submit" class="btn btn-primary">Create</button>
            <a href="{{ route('clients.index') }}" class="btn btn-danger">Cancel</a>
        </div>
    </div>
</form>
@endsection
