@extends('layouts.default')

@section('content')

<div class="buttonRow">
    <div class="text-right">
        <a href="{{ route('clients.index') }}" class="btn btn-danger mb-2">Go Back</a>
    </div>
</div>
<hr />

<form action="{{ route('clients.update', $client_info->id) }}" method="POST" name="update_product">
  {{ csrf_field() }}
  @method('PATCH')

  <div class="row">
      <div class="col-md-12">
          <div class="form-group">
              <strong>Name</strong>
              <input type="text" name="name" class="form-control" placeholder="Enter Title" value="{{ $client_info->name }}">
              <span class="text-danger">{{ $errors->first('name') }}</span>
          </div>
      </div>
      <div class="col-md-12">
          <div class="form-group">
              <strong>Postal Code</strong>
              <input type="text" name="address" class="form-control" placeholder="Enter Postal Code" value="{{ $client_info->postalCode }}">
              <span class="text-danger">{{ $errors->first('postalCode') }}</span>
          </div>
      </div>
      <div class="col-md-12">
                <div class="form-group">
                    <strong>Address</strong>
                    <input type="text" name="address" class="form-control" placeholder="Enter Address" value="{{ $client_info->address }}">
                    <span class="text-danger">{{ $errors->first('address') }}</span>
                </div>
            </div>
      <div class="col-md-12">
          <button type="submit" class="btn btn-primary">Submit</button>
          <a href="{{ route('clients.index') }}" class="btn btn-danger">Cancel</a>
      </div>
  </div>

</form>
@endsection
