@section('content')

 <div class="buttonRow">
    <a href="{{ route('clients.create')}}" class="btn btn-success mb-2">Add</a>
 </div>

  <div class="">
    <table class="table table-bordered" id="laravel_crud">
     <thead>
        <tr>
           <th>Id</th>
           <th>Name</th>
            <th>Postal Code</th>
           <th>Address</th>
           <th colspan="1" class="text-center">Action</th>
        </tr>
     </thead>
     <tbody>
        @foreach($clients as $client)
        <tr>
           <td>{{ $client->id }}</td>
           <td>{{ $client->name }}</td>
            <td>{{ $client->postalCode }}</td>
           <td>{{ $client->address }}</td>
           <td class="">
           <div>
            <a href="{{ route('clients.edit',$client->id)}}" style="display:inline-block" class="btn btn-primary">Edit</a>
           <form action="{{ route('clients.destroy', $client->id)}}" style="display:inline-block"  method="post">
            {{ csrf_field() }}
            @method('DELETE')
            <button class="btn btn-danger" type="submit">Delete</button>
          </form>
          </div>
          </td>
        </tr>
        @endforeach

        @if(count($clients) < 1)
          <tr>
           <td colspan="10" class="text-center">There are no client available yet!</td>
          </td>
        </tr>
        @endif
     </tbody>
    </table>
      <div style="display:flex;justify-content:center">
          {{$clients ->links()}}
      </div>
</div>
 @endsection
