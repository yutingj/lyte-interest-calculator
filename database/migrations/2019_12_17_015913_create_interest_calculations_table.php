<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInterestCalculationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('interest_calculations', function (Blueprint $table) {
            $table->increments('id',20);
            $table->integer('elaspeDay');
            $table->date('date');
            $table->decimal('amountDue', 50, 2);
            $table->decimal('principal', 20,2);
            $table->decimal('minInterest', 20,2);
            $table->decimal('earlyInterest', 20,2);
            $table->decimal('lateInterest', 20,2);
            $table->decimal('totalCheck', 20,2);
            $table->string('invoiceID', 20);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('interest_calculations');
    }
}
