<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvoicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invoices', function (Blueprint $table) {
            $table->string('id',20);
            $table->date('date');
            $table->decimal('amount', 10, 2);
            $table->integer('term');
            $table->date('dueDate');
            $table->date('purchaseDate');
            $table->decimal('financePerc', 20,2);
            $table->decimal('advanceAmt', 20,2);
            $table->decimal('graceIntRate', 20,2);
            $table->decimal('lateIntRate', 20,2);
            $table->integer('minPeriod');
            $table->date('minDate');
            $table->integer('gracePeriod');
            $table->date('paymentDueDate');
            $table->date('defaultDue');
            $table->integer('latePPeriod');
            $table->bigInteger('clientID');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invoices');
    }
}
