<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InterestCalculation extends Model
{
    public $fillable = [
        'elaspeDay',
        'amountDue',
        'principal',
        'minInterest',
        'earlyInterest',
        'lateInterest',
        'totalCheck',
        'date',
        'invoiceID',
    ];
}
