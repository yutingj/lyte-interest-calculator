<?php
 namespace App;
 use Illuminate\Database\Eloquent\Model;
 class Client extends Model
 {
    protected $fillable = [
     'name',
     'address',
        'postalCode',
    ];

     protected $visible = ['name', 'address'];

     protected $primaryKey = 'id';
 }
