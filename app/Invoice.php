<?php

namespace App;

use App\Client;
use Illuminate\Database\Eloquent\Model;

class Invoice extends Model
{
    protected $fillable = [
        'id',
        'date',
        'amount',
        'term',
        'dueDate',
        'purchaseDate',
        'financePerc',
        'advanceAmt',
        'graceIntRate',
        'lateIntRate',
        'minPeriod',
        'minDate',
        'gracePeriod',
        'paymentDueDate',
        'defaultDue',
        'latePPeriod',
        'clientID',
    ];

    public $incrementing = false;

    protected $visible = [
        'id',
        'date',
        'amount',
        'term',
        'dueDate',
        'purchaseDate',
        'financePerc',
        'advanceAmt',
        'graceIntRate',
        'lateIntRate',
        'minPeriod',
        'minDate',
        'gracePeriod',
        'paymentDueDate',
        'defaultDue',
        'latePPeriod',
        'clientID',
    ];

    protected $dates = [
        'created_at',
        'updated_at',
        'date',
        'dueDate',
        'minDate',
        'minDate',
        'paymentDueDate',
        'defaultDue',
        'purchaseDate'
    ];

    protected $primaryKey = 'id';

    public function client()
    {
        return $this->hasOne('App\Client', 'id', 'clientID');
    }

}
