<?php

namespace App\Http\Controllers;

use \Datetime;
use Schema;
use Redirect;
use DB;
use Illuminate\Support\Facades\Input;
use App\InterestCalculation;
use Carbon\Carbon;
use App\Invoice;
use App\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;


class InvoiceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
        {
            $data['title'] = 'Invoices';
            $data['clients'] = Client::orderBy('id', 'asc')->get();

            $data['invoices'] = DB::table('invoices')
                ->join('clients', 'invoices.clientID', '=', 'clients.id')
                ->select('clients.id','clients.name','invoices.id','invoices.date','invoices.amount','invoices.dueDate','invoices.advanceAmt','invoices.paymentDueDate')
                ->paginate(10);
            return view('invoice.index',$data);
        }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['title'] = 'Create Invoice';
        $data['clients'] = Client::orderBy('id', 'asc')->get();
        return view('invoice.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validation = Validator::make($request->all(),[
            'id' => 'required|unique:invoices',
            'date' => 'required|date_format:d/m/Y',
            'amount' => 'required|numeric|max:1000000000',
            'term' => 'required|numeric|max:10000',
            'dueDate'=> 'required|date_format:d/m/Y',
            'purchaseDate'=>'required|date_format:d/m/Y',
            'financePerc' => 'required|numeric|max:100',
            'advanceAmt' => 'required|numeric|max:10000000',
            'graceIntRate' => 'required|numeric|max:100',
            'lateIntRate' => 'required|numeric|max:100',
            'minPeriod' => 'required|numeric|max:10000',
            'minDate' => 'required|date_format:d/m/Y',
            'gracePeriod' => 'required|numeric|max:1000',
            'paymentDueDate' => 'required|date_format:d/m/Y',
            'defaultDue' => 'required|date_format:d/m/Y',
            'latePPeriod' => 'required|numeric',
            'clientID' => 'required',
        ]);


        if ($validation->fails()) {
            return Redirect::back()
                ->withErrors($validation)
                ->withInput();
        }

        app('App\Http\Controllers\InterestCalculationController')->store($request);


        //get date from request
        $date1 = $request->get('dueDate');
        //add the converted date to request
        $request->request->add(['dueDate'=> DateTime::createFromFormat('m/d/Y', $date1)->getTimestamp()]);

        //get date from request
        $date1 = $request->get('purchaseDate');
        //add the converted date to request
        $request->request->add(['purchaseDate'=> DateTime::createFromFormat('m/d/Y', $date1)->getTimestamp()]);

        $date1 = $request->get('minDate');
        //add the converted date to request
        $request->request->add(['minDate'=> DateTime::createFromFormat('m/d/Y', $date1)->getTimestamp()]);

        $date1 = $request->get('paymentDueDate');
        //add the converted date to request
        $request->request->add(['paymentDueDate'=> DateTime::createFromFormat('m/d/Y', $date1)->getTimestamp()]);

        $date1 = $request->get('defaultDue');
        //add the converted date to request
        $request->request->add(['defaultDue'=> DateTime::createFromFormat('m/d/Y', $date1)->getTimestamp()]);

        $date1 = $request->get('date');
        //add the converted date to request
        $request->request->add(['date'=> DateTime::createFromFormat('m/d/Y', $date1)->getTimestamp()]);

        Invoice::create($request->all());

        return Redirect::to('invoices')
            ->with('success','Great! invoice created successfully.');


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
         $where = array('id' => $id);
         $data['invoice_info'] = invoice::where($where)->first();
         $data['title'] = 'View Invoice';
         $data['interestCalculations'] = DB::table('interest_calculations')
            ->join('invoices', 'interest_calculations.invoiceID', '=', 'invoices.id')
            ->select('interest_calculations.date',
                'interest_calculations.elaspeDay',
                'interest_calculations.id',
                'interest_calculations.date',
                'interest_calculations.amountDue',
                'interest_calculations.principal',
                'interest_calculations.minInterest',
                'interest_calculations.earlyInterest',
                'interest_calculations.lateInterest',
                'interest_calculations.totalCheck'
                )
             ->where('invoiceID','=',$id)
             ->orderBy('interest_calculations.date','asc')
             ->paginate(15);

         return view('invoice.view', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
         $where = array('id' => $id);
         $data['invoice_info'] = invoice::where($where)->first();
         $data['title'] = 'Edit Invoice';
         $data['client'] = Client::where(array('id' => $data['invoice_info']->clientID))->first();


        return view('invoice.edit', $data);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $validation = Validator::make($request->all(),[
            'date' => 'required|date_format:d/m/Y',
            'amount' => 'required|numeric|max:1000000000',
            'term' => 'required|numeric|max:10000',
            'dueDate'=> 'required|date_format:d/m/Y',
            'purchaseDate'=>'required|date_format:d/m/Y',
            'financePerc' => 'required|numeric',
            'advanceAmt' => 'required|numeric',
            'graceIntRate' => 'required|numeric',
            'lateIntRate' => 'required|numeric',
            'minPeriod' => 'required|numeric',
            'minDate' => 'required|date_format:d/m/Y',
            'gracePeriod' => 'required|numeric',
            'paymentDueDate' => 'required|date_format:d/m/Y',
            'defaultDue' => 'required|date_format:d/m/Y',
            'latePPeriod' => 'required|numeric',
        ]);

        if ($validation->fails()) {
            return Redirect::back()
                ->withErrors($validation)
                ->withInput();
        }

        Invoice::where('id',$id)->update(
            [
                "amount"=> $request->amount,
                "term" => $request->term,
                "dueDate" => Carbon::createFromFormat('d/m/Y',$request->dueDate)->format('Y-m-d H:i:s'),
                "purchaseDate" => Carbon::createFromFormat('d/m/Y',$request->purchaseDate)->format('Y-m-d H:i:s'),
                "financePerc" => $request->financePerc,
                "advanceAmt" => $request->advanceAmt,
                "graceIntRate" => $request->graceIntRate,
                "lateIntRate" => $request->lateIntRate,
                "minPeriod" => $request->minPeriod,
                "minDate" => Carbon::createFromFormat('d/m/Y',$request->minDate)->format('Y-m-d H:i:s'),
                "gracePeriod" => $request->gracePeriod,
                "paymentDueDate" => Carbon::createFromFormat('d/m/Y',$request->paymentDueDate)->format('Y-m-d H:i:s'),
                "defaultDue" => Carbon::createFromFormat('d/m/Y',$request->defaultDue)->format('Y-m-d H:i:s'),
                "latePPeriod" => $request-> latePPeriod,
                "date" => Carbon::createFromFormat('d/m/Y',$request->date)->format('Y-m-d H:i:s'),
            ]
        );


        InterestCalculation::where('invoiceID', $id)->delete();
        app('App\Http\Controllers\InterestCalculationController')->store($request);


        return Redirect::to('invoices')
       ->with('success','Great! Invoice updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
         Invoice::where('id',$id)->delete();
         InterestCalculation::where('invoiceID', $id)->delete();

         return Redirect::to('invoices')->with('success','Invoice deleted successfully');
    }

}
