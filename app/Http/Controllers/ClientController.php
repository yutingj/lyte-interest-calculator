<?php

namespace App\Http\Controllers;

use Redirect;
use App\Client;
use Illuminate\Http\Request;

class ClientController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
        {
            $data['clients'] = Client::orderBy('id','desc')->paginate(10);
            $data['title'] = 'Clients';

            return view('client.index',$data);
        }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
        {
            $data['title'] = 'Add Client';
            return view('client.create', $data);
        }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'address' => 'required',
            'postalCode' => 'required',
        ]);

        Client::create($request->all());

        return Redirect::to('clients')
       ->with('success','Great! Client created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
         $where = array('id' => $id);
         $data['client'] = Client::where($where)->first();

         return view('client.edit', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
         $where = array('id' => $id);
         $data['client_info'] = Client::where($where)->first();
         $data['title'] = 'Edit Client';

         return view('client.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         $request->validate([
            'name' => 'required',
            'address' => 'required',
             'postalCode' => 'postalCode',
        ]);

        $update = ['name' => $request->name, 'address' => $request->address];
        Client::where('id',$id)->update($update);

        return Redirect::to('clients')
       ->with('success','Great! Client updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
         Client::where('id',$id)->delete();

         return Redirect::to('clients')->with('success','Client deleted successfully');
    }
}
