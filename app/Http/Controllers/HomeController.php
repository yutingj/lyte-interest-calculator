<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Carbon\Carbon;

class HomeController extends Controller
{
    //

    public function index()
    {
        $data['title'] = 'Welcome back';
        $data['invoices'] = DB::table('invoices')
            ->join('clients', 'invoices.clientID', '=', 'clients.id')
            ->select('clients.id','clients.name','invoices.id','invoices.date','invoices.amount','invoices.dueDate','invoices.advanceAmt','invoices.paymentDueDate')
            ->orderBy('dueDate','asc')
            ->where('dueDate','>',Carbon::now())
            ->paginate(10);
        return view('home.index',$data);
    }
}
