<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Support\Facades\Redirect;
use App\InterestCalculation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class InterestCalculationController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */



    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Create the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $paymentDueDate = Carbon::createFromFormat('d/m/yy',$request->paymentDueDate);

        $advanceAmt = $request->get('advanceAmt');
        $minPeriod = $request->get('minPeriod');
        $graceIntRate = $request->get('graceIntRate')/100;
        $lateIntRate = $request->get('lateIntRate')/100;

        $principal = $advanceAmt;
        $minInterest = ($minPeriod * ($graceIntRate/30) * $advanceAmt);


        for ($x = 1; $x <= 214; $x++) {
            $elaspeDay = $x;
            if ($x == 1) {
                $generatedDate = Carbon::createFromFormat('d/m/yy',$request->purchaseDate);
            } else {
                $generatedDate = Carbon::createFromFormat('d/m/yy',$request->purchaseDate)->addDays($x);
            }

            // Amount Due
            if ($elaspeDay < $minPeriod + 1) {
                $amountDue = ($minPeriod * ($graceIntRate/30) * $advanceAmt + $advanceAmt);
            } else {
                if ($generatedDate < Carbon::createFromFormat('d/m/yy',$request->paymentDueDate)->addDays(1)) {
                    $amountDue = $elaspeDay * ($graceIntRate/30) * $advanceAmt + $advanceAmt;
                } else {
                    $amountDue = $elaspeDay * ($graceIntRate/30) * $advanceAmt +
                        ($generatedDate->diffInDays($paymentDueDate)*($lateIntRate/30)*$advanceAmt) + $advanceAmt;
                }
            }

            //early Interest

            if($elaspeDay < $minPeriod + 1) {
                $earlyInterest = 0;
            } else {
                $earlyInterest = ($elaspeDay - $minPeriod) * ($graceIntRate/30) * $advanceAmt;
            }

            //late Interest

            if ($generatedDate < Carbon::createFromFormat('d/m/yy',$request->paymentDueDate)->addDays(1)) {
                $lateInterest = 0;
            } else {
                $lateInterest = ($generatedDate->diffInDays($paymentDueDate))*($lateIntRate/30)*$advanceAmt;
            }

            $totalCheck = $principal + $minInterest + $earlyInterest + $lateInterest;

            InterestCalculation::create([
                "invoiceID" => $request->id,
                "amountDue" => $amountDue,
                "elaspeDay" => $x,
                "principal" => $principal,
                "minInterest" => $minInterest,
                "earlyInterest" => $earlyInterest,
                "lateInterest" => $lateInterest,
                "totalCheck" => $totalCheck,
                "date" => Carbon::parse($generatedDate)->format('Y-m-d H:i:s'),
            ]);
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
