<?php

namespace App\Http\Controllers;

use App\Client;
use App\Invoice;
use Schema;
use Redirect;
use DB;
use Carbon\Carbon;
use App;
use Illuminate\Http\Request;
use Resource\htmlFiles\docTemplate;
use Dompdf\Dompdf;

class DownloadPDFController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function download($id)
    {
        $data['title'] = 'Invoices';
        $data['clients'] = Client::orderBy('id', 'asc')->get();

        $data['invoice_info'] = DB::table('invoices')
            ->join('clients', 'invoices.clientID', '=', 'clients.id')
            ->select('clients.id',
                'clients.name',
                'invoices.id',
                'invoices.date',
                'invoices.amount',
                'invoices.dueDate',
                'invoices.advanceAmt',
                'invoices.paymentDueDate',
                'invoices.graceIntRate',
                'invoices.lateIntRate',
                'invoices.term',
                'invoices.gracePeriod',
                'invoices.purchaseDate',
                'invoices.financePerc'
                )
            ->where('invoices.id','=', $id)
            ->first();

        $data['dateToday'] = "dateToday";

        $paymentDueDate = DB::table('invoices')->select('paymentDueDate')->where('id','=',$id)->first();
        $paymentDueDate = DB::table('invoices')->select('paymentDueDate')->where('id','=',$id)->first();
        $minDate = DB::table('invoices')->select('minDate')->where('id','=',$id)->first();


        $data['minDate'] = $minDate;
        $data['thirtyDays'] = DB::table('interest_calculations')
            ->join('invoices', 'interest_calculations.invoiceID', '=', 'invoices.id')
            ->select('interest_calculations.date','interest_calculations.amountDue')
            ->where('invoiceID','=',$id)
            ->where('interest_calculations.date','=',$minDate->minDate)
            ->limit(1)
            ->get();

        $data['beforeLateRepayments'] = DB::table('interest_calculations')
            ->join('invoices', 'interest_calculations.invoiceID', '=', 'invoices.id')
            ->select('interest_calculations.date','interest_calculations.amountDue')
            ->where('invoiceID','=',$id)
            ->where('interest_calculations.date','<=',$paymentDueDate->paymentDueDate)
            ->orderBy('interest_calculations.date','desc')
            ->limit(5)
            ->get()->reverse();

        $data['lateRepayments'] = DB::table('interest_calculations')
            ->join('invoices', 'interest_calculations.invoiceID', '=', 'invoices.id')
            ->select('interest_calculations.date','interest_calculations.amountDue')
            ->where('invoiceID','=',$id)
            ->where('interest_calculations.date','>',$paymentDueDate->paymentDueDate)
            ->orderBy('interest_calculations.date','asc')
            ->limit(5)
            ->get();

        $dompdf = new DOMPDF();
        $dompdf->loadHtml(view('invoice.docTemplate', $data));
        $dompdf->render();
        $dompdf->stream("sample.pdf", array("Attachment"=>0));

    }


}

