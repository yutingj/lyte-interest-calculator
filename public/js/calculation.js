$(document).ready(function () {
    var x = jQuery.noConflict();
});




function calculateMinDate(date) {
    var editedDate2 = moment(date,"DD/MM/YYYY");
    editedDate2.add(29 , 'days');
    jQuery("#minDate").val(editedDate2.format("DD/MM/YYYY"));
}


$(document).on('change', '#term', function() {
    var date = jQuery("#date").val();
    var term = jQuery("#term").val();
    calculateDueDates(date,term);
});


$(document).on('change', '#gracePeriod', function() {
    var date = jQuery("#dueDate").val();
    var gracePeriod = jQuery("#gracePeriod").val();
    calculatePaymentDueDate(date,gracePeriod);
    calculateDefaultDueDate();
});


$(document).on('change', '#amount', function() {
    // Action goes here.
    var amount = jQuery("#amount").val();
    var financePerc = jQuery("#financePerc").val()/100;
    calculateAmount(amount,financePerc);
});


$(document).on('change', '#financePerc', function() {
    var amount = jQuery("#amount").val();
    var financePerc = jQuery("#financePerc").val()/100;
    calculateAmount(amount,financePerc);
});

$(document).on('change', '#purchaseDate', function() {
    var date = jQuery("#purchaseDate").val();
    calculateMinDate(date);
    calculateDefaultDueDate();
});
$(document).on('change', '#latePPeriod', function() {
    calculateDefaultDueDate();
});


function calculatePaymentDueDate(date,period) {
    var newDate1 = moment(date,"DD/MM/YYYY");
    newDate1.add(period-1,"days");
    jQuery("#paymentDueDate").val(newDate1.format('DD/MM/YYYY'));
    calculateDefaultDueDate();
}

function calculateDefaultDueDate() {
    var paymentDueDate = moment(jQuery("#paymentDueDate").val(),"DD/MM/YYYY");
    var latePPeriod = jQuery("#latePPeriod").val();
    paymentDueDate.add(latePPeriod,"days");
    jQuery("#defaultDue").val(paymentDueDate.format('DD/MM/YYYY'));
}


function calculateAmount(amount,interest) {
    var advanceAmt = amount*interest;
    jQuery("#advanceAmt").val(roundOff(advanceAmt,2));
}


function calculateDueDates(date,term) {
    var editedDate = moment(date,"DD/MM/YYYY");
    editedDate.add(term,"days");
        jQuery("#dueDate").val(editedDate.format('DD/MM/YYYY'));
    var gracePeriod = jQuery("#gracePeriod").val();
    calculatePaymentDueDate(editedDate,gracePeriod);
    calculateDefaultDueDate();
}

function roundOff(number, precision) {
    num = number;
    return num.toFixed(2);
}
